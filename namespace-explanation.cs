// A namespace is basically just something that wraps your code and makes it so that anything declared
// within that namespace is unique to that namespace. Example:

// When you use a 'using' statement like this
// what you are really doing is giving you access
// to everything that was declared inside that namespace.

using System;

public class ProgramOne {

    public static void Main() {

        // The 'System' namespace has a class in it named 'Console'.
        Console.WriteLine( "Hello" );

        // The output will be the expected 'Hello'

        // Now, write down this program yourself, except comment out 'using System'.
        // If you are using visual studio you will immediatly get a little red squiggle line
        // under 'Console.WriteLine'. When I try to compile the code on the command line
        // I get the following error: "error CS0103: The name 'Console' does not exist in the current context"
        // These issues are created because when you don't use the 'using System' statement, you are not
        // including the namespace in which the Console class was defined.
        // So, whenever you see a bunch of 'using' statements at the top of a C# source file, you know that the code
        // will be using some other code which was defined in those namespaces.
    }
}



// Like I said above anything defined inside of a namespace is unique to that namespace. Example:
namespace MyConsoleNamespace {

    public class Console {

        public static void WriteLine( string message ) {

            System.Console.WriteLine( "Called from Console class in MyConsoleNamespace\n" + message );
        }
    }

    /**** NOTE:
    Since there are two Main methods in this file you need to tell the compiler which Main is the one you want 
    and I am not exactly sure how to do that in Visual Studio. You can either look it up or comment out the
    ProgramOne class from above.
    ****/

    public class ProgramTwo {

        /**** NOTE:
        Since there are two Main methods in this file you need to specify which Main is the one you want 
        and I am not exactly sure how to do that in Visual Studio. You can either look it up or comment out the
        ProgramOne class from above.
        ****/
        public static void Main() {

            Console.WriteLine( "Hello" );
        }

        // When ProgramTwo runs the output is:
        //
        // "Called from Console class in MyConsoleNamespace"
        // "Hello"
        //
        // Do you see why that is?
        //
        // Inside the namespace MyConsoleNamespace, I declares a class called Console
        // that has a method called WriteLine. In ProgramTwo.Main, when I write 'Console.WriteLine'
        // the compiler is using the Console class from the namespace in which the Main method is in, and 
        // NOT the Console class which is declared in the System namespace. 
        // You will notice that in MyConsoleNamespace.Console.WriteLine I write the code
        // 'System.Console.WriteLine'. What that code is saying is, 'In the namespace System, in the class Console,
        // call the method named WriteLine. 
        // When inside the MyConsoleNamespace, I need to write it this way, since the only Console class
        // that MyConsoleNamespace knows about is the Console class that I defined. 
        // In fact, if you just write 'Console.WriteLine' instead of 'System.Console.WriteLine', the program will not
        // run correctly. I think it just runs foreever without doing anything. That is because it is calling the
        // Console.WriteLine that I defined, which is calling the Console.WriteLine which I defined which is calling the 
        // Console.WriteLine which I defined, etc. This is called recursion, and this a a good example of how
        // not to write a recursive function lol. 
    }
} 

// Do these examples make sense to you, or do they help? 
// Let me know if you have any questions or need anything cleared up.
